#test to check that the compression is lossless
f1 = input("Enter the name of the first file:")
f2 = input("Enter the name of the second file:")

bool = True
with open(f1, 'r') as t1, open(f2, 'r') as t2:
    fileone = t1.readlines()
    filetwo = t2.readlines()

    for line in filetwo:
        if line not in fileone:
            print("Difference: " + line)
            bool = False



if (bool == True): 
    print(" ")
    print("Decompressed file is a 100% match.\n")
 
else: 
    print(" ")
    print("Some data was lost in the compression process.\n")
  