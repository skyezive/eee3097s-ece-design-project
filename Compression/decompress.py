import bz2, os, time

input_file = input("Enter a file to decompress: \n")
output_file = input("Enter the desried name of the decompressed file: \n")

startDecompression = time.time()
with bz2.open(input_file, "rb") as fout: #decompressing data
    # Decompress data from file
    decompressedFile = fout.read()   
finishDecompression = time.time()
decompressionTime = finishDecompression - startDecompression    
     
with open(output_file, "wb") as decomp: #writing decompressed data to output file
    # Write compressed data to file
    decomp.write(decompressedFile)
    

print(" ")
print("Decompression complete!")
print("Execution Time for Decompression:", round(decompressionTime, 4), "seconds")