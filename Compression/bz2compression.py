import bz2, os, time

input_file = input("Enter a file to compress: \n")
output_file = input("Enter the desried name of the compressed file: \n (File extension must be .bz2) \n ")

with open(input_file, 'rb') as file: #reading in original data file
    originalFile = file.read()

startCompression = time.time()
with bz2.open(output_file, "wb") as fin: #compressing data
    # Write compressed data to file
    fin.write(originalFile)
finishCompression = time.time()
compressionTime = finishCompression-startCompression

    
uncompressed = os.path.getsize(input_file)
compressed = os.path.getsize(output_file)

print(" ")
print(f"Size of original file:" , uncompressed, "bytes" )
print(f"Size of compressed file:", compressed, "bytes" )
print(f"Compression Ratio:", round(uncompressed/compressed, 4))
print("Execution Time:", round(compressionTime, 4), "seconds")
