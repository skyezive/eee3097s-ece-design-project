# EEE3097S ECE Design Project

## Overview
Our project involves the IMU sensor on the SHARC Buoy project. The crucial sensor gives
information about the ice as well as wave dynamics. Ideally, we would like as much of the IMU
data as possible but transmitting the data using Iridium is extremely costly. Therefore, this
project will be undertaken with the specific goals to compress and encrypt the IMU data.

This project has been developed to implement simple compression and encryption algorithms. The code we currently have is able to compress and encrypt the csv files which are given in the 'Data' directory, but we aim to be able to process data directly from the ICM20649 sensor.

## **How to Run the Compression Algorithms**
No packages need to be installed for the compression process. 
Run the bz2compression.py file to begin the compressioin. You will be asked for the name of a file to compress - enter this into the command line. The compressed file will have a file extension ".bz2". 

Run the decompress.py file to decompress the file. You will again be prompted to  enter the name of the file you would like to decompress, and the name of the output decompressed file. The decompressed file should have the same file extension as the origional uncompressed file.

In order to check that the compression was lossless, you can run the checkContents.py file. This will print either "The decompressed file is a 100% match." if this is the case, or it will print "Some data was lost in the compression process." and will then list the differences between the two files.

## **How to Run the Encryption Algorithms**
Run the CryptographyEncryption.py code to encrypt the file. You will be prompted to enter the name of the file you wish to encrypt. Do so, and press enter.

To decrypt the file, run CryptographyDecryption.py. You will be prompted to enter the name of the file you wish to decrypt. Once decryption has taken place, you can verify that it matches the orinal file by running checkContents.py.


### This project is licensed under the terms of the MIT license.



